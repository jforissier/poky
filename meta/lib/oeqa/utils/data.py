# Copyright (C) 2022 Linaro Limited
#
# SPDX-License-Identifier: MIT

import os
import json

from oeqa.core.utils.test import getCaseID, getCaseFile, getCaseMethod


def get_data(self, key = None):
    """get_data() returns test case specific data to the test case implementation.
    data is stored in image specific json file called "testimage_data.json" in
    image deploy directory. Data matching test method name and an optional key
    is returned to the test case. This data can then be used by generic test
    cases to match image specific functionality and expected behavior. For example
    list of expected kernel error strings, list of active systemd services etc.
    can be image specific while the test case implementation to check them is
    generic. Example json file for runtime test parselogs.py to ignore image
    specific kernel error strings in dmesg:

    {"test_parselogs":{"ignore_errors":[
        "Error to be ignored in dmesg"
    ]}}
    """
    test_method = getCaseMethod(self)
    self.logger.info("%s: get_data() called by test_method =  %s, key = %s" % (__file__, test_method, key))

    json_file_name = os.path.join(self.td['DEPLOY_DIR_IMAGE'], "testimage_data.json")
    self.logger.debug("%s: json_file_name = %s" % (__file__, json_file_name))

    with open(json_file_name) as json_file:
        self.logger.debug("%s: json_file = %s" % (__file__, json_file))
        json_data = json.load(json_file)
        self.logger.debug("%s: json_data = %s" % (__file__, json_data))
        if key:
            data = json_data[test_method][key]
        else:
            data = json_data[test_method]
        self.logger.debug("%s: data = %s" % (__file__, data))
        return data
